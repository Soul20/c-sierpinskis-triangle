# C Sierpinskis Triangle

C program to create a Sierpinski Triangle

## Compiling
This program uses the math library, Link math library by using -lm while compiling if you are using Linux or macOS.

## Usage
./a.out \<int total-iterations\> \<int triangle-width\>
<pre>./a.out 3 5
                                             A    
                                            AAA   
                                           AAAAA  
                                          AAAAAAA 
                                         AAAAAAAAA
                                        A         A    
                                       AAA       AAA   
                                      AAAAA     AAAAA  
                                     AAAAAAA   AAAAAAA 
                                    AAAAAAAAA AAAAAAAAA
                                   A                   A    
                                  AAA                 AAA   
                                 AAAAA               AAAAA  
                                AAAAAAA             AAAAAAA 
                               AAAAAAAAA           AAAAAAAAA
                              A         A         A         A    
                             AAA       AAA       AAA       AAA   
                            AAAAA     AAAAA     AAAAA     AAAAA  
                           AAAAAAA   AAAAAAA   AAAAAAA   AAAAAAA 
                          AAAAAAAAA AAAAAAAAA AAAAAAAAA AAAAAAAAA
                         A                                       A    
                        AAA                                     AAA   
                       AAAAA                                   AAAAA  
                      AAAAAAA                                 AAAAAAA 
                     AAAAAAAAA                               AAAAAAAAA
                    A         A                             A         A    
                   AAA       AAA                           AAA       AAA   
                  AAAAA     AAAAA                         AAAAA     AAAAA  
                 AAAAAAA   AAAAAAA                       AAAAAAA   AAAAAAA 
                AAAAAAAAA AAAAAAAAA                     AAAAAAAAA AAAAAAAAA
               A                   A                   A                   A    
              AAA                 AAA                 AAA                 AAA   
             AAAAA               AAAAA               AAAAA               AAAAA  
            AAAAAAA             AAAAAAA             AAAAAAA             AAAAAAA 
           AAAAAAAAA           AAAAAAAAA           AAAAAAAAA           AAAAAAAAA
          A         A         A         A         A         A         A         A    
         AAA       AAA       AAA       AAA       AAA       AAA       AAA       AAA   
        AAAAA     AAAAA     AAAAA     AAAAA     AAAAA     AAAAA     AAAAA     AAAAA  
       AAAAAAA   AAAAAAA   AAAAAAA   AAAAAAA   AAAAAAA   AAAAAAA   AAAAAAA   AAAAAAA 
      AAAAAAAAA AAAAAAAAA AAAAAAAAA AAAAAAAAA AAAAAAAAA AAAAAAAAA AAAAAAAAA AAAAAAAAA
</pre>
