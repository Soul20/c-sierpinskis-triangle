#include "stdio.h"
#include "stdlib.h"
#include "math.h"

// function to return Pascal's triangle of n layers
int** pascalsTriangle (int n){
	int i;

	// initilise 2d array
	int** matrix;
	matrix = malloc(sizeof(int*) * n); // malloc returns a memory block of given size and returns a pointer to the beggining of the block
	for (i=0; i<n; i++) matrix[i] = calloc(n*n, sizeof(int)); // calloc allocates the memory and also initliises every byte in the allocated memory to 0

	// calculate Pascal's triangle
	matrix [0][0] = 1;
	for (i=1; i<n; i++){
		for (int ii=0; ii<n; ii++){
			int prevN = i-1;
			matrix[i][ii] = matrix[prevN][ii] + (ii==0 ? 0 : matrix[prevN][ii-1]);
		}
	}

	return matrix;
}

void freeMatrix (int ** matrix){
	free(*matrix); free(matrix);
}

void render (char c, int l){
	for (int i=0; i<l; i++) printf("%c",c);
}

int numBricksIn (int n){
	return (n*2)+1;
}

int main(int argc, char**argv){
	char airChar = ' ', brickChar = 'A';

	int n = pow(2, atoi(argv[1]));
	int triangleSize = atoi(argv[2]), triangleWidth = numBricksIn(triangleSize);

	int ** matrix = pascalsTriangle(n);

	for (int i=0; i<n; i++){ // for ever layer
		for (int ii=0; ii<triangleSize; ii++){ // for every line in the triangle

			int offset = (n - i) * triangleSize;
			render(airChar, offset);

			for (int k=0; k<i+1; k++){ // for each column
				int numBricksInCurrentLayer = numBricksIn(ii);
				int padding = (triangleWidth - (numBricksInCurrentLayer))/2;

				render(airChar, padding);
				render(matrix[i][k]%2==0 ? airChar : brickChar, numBricksInCurrentLayer);
				render(airChar, padding - 1);
			}

			printf("\n");
		}
	}

	free(matrix);
	return 0;
}
